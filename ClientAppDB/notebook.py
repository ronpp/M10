from client import Client
import db


def get_client_list(order=False):
    query = "SELECT * FROM client"
    if order:
        query = "SELECT * FROM client ORDER BY name"
    return db.select_query(query)


def add_client(name, lastname, phone, email, address, city):
    client = Client(name, lastname, phone, email, address, city)
    query = f"""
    INSERT INTO client(name, lastname, phone, email, address, city) 
    VALUES('{name}', '{lastname}', '{phone}', '{email}', '{address}', '{city}')
    """
    db.execute_sql_query(query)


def update_client(client_id, client: Client):
    query = f"""
    UPDATE client SET name = '{client.name}', lastname = '{client.lastname}', phone = '{client.phone}',
    email = '{client.email}', address = '{client.address}', city = '{client.city}'
    WHERE id = {client_id}
    """
    db.execute_sql_query(query)


def delete_client(client_id):
    db.delete_field(client_id)


def find_client_by_id(client_id):
    query = f"SELECT * FROM client WHERE id = '{client_id}'"
    data = db.select_query(query)
    if data:
        data = db.select_query(query)[0]
        client = Client(data[1], data[2], data[3], data[4], data[5], data[6])
        client.set_client_id(data[0])
        return client
    return f"the client with id '{client_id}' don't exist"


def find_client_by_name(name):
    query = f"SELECT * FROM client WHERE name ILIKE '%{name}%'"
    data = db.select_query(query)
    if data:
        if len(data) == 1:
            data = db.select_query(query)[0]
            client = Client(data[1], data[2], data[3], data[4], data[5], data[6])
            client.set_client_id(data[0])
            return client
        else:
            clients = []
            for cl in data:
                client = Client(cl[1], cl[2], cl[3], cl[4], cl[5], cl[6])
                client.set_client_id(cl[0])
                clients.append(client)
            return clients
    return f"the client with name '{name}' don't exist"


def find_client_by_lastname(lastname):
    query = f"SELECT * FROM client WHERE lastname ILIKE '%{lastname}%'"
    data = db.select_query(query)
    if data:
        if len(data) == 1:
            data = db.select_query(query)[0]
            client = Client(data[1], data[2], data[3], data[4], data[5], data[6])
            client.set_client_id(data[0])
            return client
        else:
            clients = []
            for cl in data:
                client = Client(cl[1], cl[2], cl[3], cl[4], cl[5], cl[6])
                client.set_client_id(cl[0])
                clients.append(client)
            return clients
    return f"the client with lastname '{lastname}' don't exist"


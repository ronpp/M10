import psycopg2


def connectio_db():
    PSQL_HOST = 'localhost'
    PSQL_PORT = '5432'
    PSQL_USER = 'iam49949708'
    PSQL_PASS = '123456'
    PSQL_DB = 'm10'
    connection_address = f"host={PSQL_HOST} port={PSQL_PORT} user={PSQL_USER} password={PSQL_PASS} dbname={PSQL_DB}"
    connection = psycopg2.connect(connection_address)
    return connection


def create_table(table_name):
    conn = connectio_db()
    cursor = conn.cursor()
    sql = f"""
    CREATE TABLE IF NOT EXISTS {table_name}(
    id SERIAL PRIMARY KEY,
    name varchar(20),
    lastname varchar(20),
    phone varchar(10),
    email varchar(20),
    address varchar(50),
    city varchar(20) )
    """
    execute_sql_query(sql)


def select_query(query):
    conn = connectio_db()
    cursor = conn.cursor()
    cursor.execute(query)
    data = cursor.fetchall()
    cursor.close()
    conn.close()
    return data


def delete_field(id_field):
    query = f"DELETE FROM client where id = '{id_field}'"
    execute_sql_query(query)


def execute_sql_query(query):
    conn = None
    result = None
    try:
        conn = connectio_db()
        cursor = conn.cursor()
        cursor.execute(query)
        result = cursor.statusmessage
        cursor.close()
        conn.commit()
    except(Exception, psycopg2.DatabaseError) as error:
        conn.rollback()
        result = error
    finally:
        if conn is not None:
            conn.close()
    return result







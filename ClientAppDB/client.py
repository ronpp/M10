class Client:
    def __init__(self, name: str, lastname: str, phone: str, email: str, address: str, city: str):
        self.id = None
        self.name = name
        self.lastname = lastname
        self.phone = phone
        self.email = email
        self.address = address
        self.city = city

    def set_client_id(self, client_id):
        self.id = client_id

    def __repr__(self):
        return repr(
            f"Id: {self.id}, name: {self.name}, lastname: {self.lastname}, phone: {self.phone}, email: {self.email}, "
            f"Address: {self.address}, city: {self.city}")

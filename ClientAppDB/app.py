import menu
from notebook import add_client, delete_client, get_client_list, find_client_by_id, find_client_by_name,\
    find_client_by_lastname, update_client

QUIT_MAIN_MENU = 5
QUIT_CONSULT_MENU = 6


def space_separator(nun_spaces: int):
    for s in range(nun_spaces):
        print()


space_separator(1)
option_main_menu = menu.show_main_menu()
while option_main_menu != QUIT_MAIN_MENU:

    if option_main_menu == 1:
        space_separator(1)
        print("New Client")
        name = input("client name: ")
        lastname = input("client lastname: ")
        phone = input("client phone: ")
        email = input("client email: ")
        address = input("client address: ")
        city = input("client city: ")
        add_client(name, lastname, phone, email, address, city)
        space_separator(1)
        print("Client added!!")

    elif option_main_menu == 2:
        space_separator(1)
        print("ELIMINATION OF CLIENT!! << WATCH OUT >>")
        print("Client List:")
        for client in get_client_list():
            print(client)
        space_separator(1)
        client_id = input("Enter the client ID: ")
        delete_client(client_id)
        print(f"The client with id: {client_id} has been deleted")

    elif option_main_menu == 3:
        space_separator(1)
        option_consult_menu = menu.show_consult_menu()

        while option_consult_menu != QUIT_CONSULT_MENU:

            if option_consult_menu == 1:
                space_separator(1)
                client_id = input("Enter the client ID: ")
                print(find_client_by_id(client_id))

            # Find By Name
            elif option_consult_menu == 2:
                space_separator(1)
                client_name = input("Enter the client name: ")
                data = find_client_by_name(client_name)
                if type(data) is list:
                    for client in find_client_by_name(client_name):
                        print(client)
                else:
                    print(find_client_by_name(client_name))

            # Find By Lastname
            elif option_consult_menu == 3:
                space_separator(1)
                client_lastname = input("Enter the client lastname: ")
                data = find_client_by_lastname(client_lastname)
                if type(data) is list:
                    for client in find_client_by_lastname(client_lastname):
                        print(client)
                else:
                    print(find_client_by_lastname(client_lastname))

            elif option_consult_menu == 4:
                space_separator(1)
                print("Client List:")
                for client in get_client_list():
                    print(client)

            elif option_consult_menu == 5:
                space_separator(1)
                print("Client List(Sorted by name):")
                for client in get_client_list(True):
                    print(client)

            else:
                space_separator(1)
                print("Invalid option!!!")

            space_separator(2)
            option_consult_menu = menu.show_consult_menu()

    elif option_main_menu == 4:
        space_separator(1)
        print("Client List:")
        for client in get_client_list():
            print(client)
        space_separator(1)
        client_id = input("Enter the client ID: ")
        client_to_mod = find_client_by_id(client_id)
        print(f"\tClient: {client_to_mod}")
        space_separator(1)
        option_field = input("What field do you want to modify { name, lastname, phone, email ,address , city }: ")
        field = input("Enter the new value: ")

        if option_field == "name":
            client_to_mod.name = field
        elif option_field == "lastname":
            client_to_mod.lastname = field
        elif option_field == "phone":
            client_to_mod.phone = field
        elif option_field == "email":
            client_to_mod.email = field
        elif option_field == "address":
            client_to_mod.address = field
        elif option_field == "city":
            client_to_mod.city = field
        update_client(client_id, client_to_mod)
        space_separator(1)
        print("Client updated!!")
        print(f"\t{find_client_by_id(client_id)}")

    else:
        space_separator(1)
        print("Invalid option!!!")
    space_separator(2)
    option_main_menu = menu.show_main_menu()

space_separator(1)
print("Goodbye, Thanks for visiting us!!")

valid_dni_letter = "TRWAGMYFPDXBNJZSQVHLCKE"
valid_nie_letter = {"X": 0, "Y": 1, "Z": 2}
dni = input("Introduzca su DNI: ")

while len(dni) < 9:
    print("El numero de identificación fiscal debe tener 9 dígitos")
    dni = input("Introduzca su DNI: ")


if not dni[0].isnumeric():
    strDni = [num for num in dni]
    strDni.remove(dni[0])
    nie_letter = valid_nie_letter[dni[0].upper()]
    strDni.insert(0, str(nie_letter))
    dni = ''.join(strDni);

num_dni = int(dni[0:len(dni) - 1])
letter_dni = dni[-1].upper()
letter = num_dni % 23

if letter_dni == valid_dni_letter[letter]:
    print(f"El DNI {dni} es valido")
else:
    print(f"El DNI {dni} no es un DNI valido")
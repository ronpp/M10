import math  # Para el ejercicio 10
import string  # Para el ejercicio 26


def space_separator():
    print()
    print()


# EJERCICIO 1 #
print("EJERCICIO 1: Media aritmética")
num1 = int(input("Introduce el primer numero: "))
num2 = int(input("Introduce el segundo numero: "))
print(f"La media aritmetica de los dos numeros es {(num1 + num2) / 2}")

space_separator()

# EJERCICIO 2 #
print("EJERCICIO 2: Distancias")
distance_feet_cm = float(input("Introduce una distancia en pulgadas: "))
print(f"La distancia en cm son {distance_feet_cm * 12 * 2.54} cm")

space_separator()

# EJERCICIO 3 #
print("EJERCICIO 3: Temperatura ")
celsius = float(input("Introduce una temperatrura en Celsius: "))
print(f"La temperatura en grados Fahrenheit es {(celsius * 9/5) + 32}")

space_separator()

# EJERCICIO 4 #
print("EJERCICIO 4: Minutos a Segundos")
total_seconds = int(input("Introduce la cantida de segundos: "))
minutes, seconds = divmod(total_seconds, 60)
print(f"{total_seconds} segundos son: {minutes} minutos y {seconds} segundos")

space_separator()

# EJERCICIO 5 #
print("EJERCICIO 5a: Vocales")
vocals = ["A", "E", "I", "O", "U"]
for i in vocals:
    print(f"{vocals.index(i) + 1}: '{i}', ", end="")
print()
position = int(input("Indica la posicion que quieres modificar: "))
value = input("Indica el valor que quieres poner: ")
vocals[position - 1] = value.upper()

for i in vocals:
    print(f"{vocals.index(i) + 1}: '{i}', ", end="")

print()
print()
print("EJERCICIO 5b")
for i in vocals:
    print(f"{vocals.index(i) + 1}: '{i}', ", end="")
print()
letter = input("Indica la letra que quieres modificar: ").upper()
value = input("Indica el valor que quieres poner: ")
vocals[vocals.index(letter)] = value.upper()
for i in vocals:
    print(f"{vocals.index(i) + 1}: '{i}', ", end="")

space_separator()

# EJERCICIO 6 #
print("EJERCICIO 6: Tabla de multiplicar del 7")
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
table7 = [x * 7 for x in numbers]
print(table7)


space_separator()

# EJERCICIO 7 #
print("EJERCICIO 7: Notas Alumnos")
grades = {"antonio": 7, "juan": 10, "sofia": 9, "pablo": 5}
print("De que Alumno deseas ver la nota?: ")
for name in grades.keys():
    print(f"- {name.capitalize()}")
student = input("Nombre: ").lower()
print(f"La nota de {student} es {grades[student]}")

space_separator()

# EJERCICIO 8 #
print("EJERCICIO 8: Lista de palabras")
list_one = ["hoy", "es", "un", "buen", "dia", "para", "aprender", "python"]
list_two = ["hoy", "es", "simplemente", "un", "buen", "dia", "para", "todo"]

both_list = []
only_first = []
only_second = []
some_list = []

for word in list_one:
    if word in list_two:
        both_list.append(word)
    elif word not in list_two:
        only_first.append(word)
for word in list_two:
    if word not in list_one:
        only_second.append(word)


print(f"Lista 1: {list_one}")
print(f"Lista 2: {list_two}")
print()
print(f"Palabras en las dos listas: {both_list}")
print(f"Palabras solo en la primera lista: {only_first}")
print(f"Palabras solo en la segunda lista: {only_second}")

space_separator()

# EJERCICIO 9 #
print("EJERCICIO 9: Division exacta")
dividend = int(input("Introduce dividendo: "))
divider = int(input("Introduce divisor: "))

while divider == 0:
    print("No se puede dividir por zero, vuelve a introducir el divisor")
    divider = int(input("Introduce otra vez el  divisor: "))

if dividend % divider == 0:
    print("La divicion es exacta")
else:
    print(f"La division no es exacta, sobra {dividend % divider}")

space_separator()

# EJERCICIO 10 #
print("EJERCICIO 10: Area figuras")
print("Elige la figura para calcular el Area: ")
print("1. Circulo")
print("2. Triangulo")

figure = int(input("Figura: "))
if figure == 1:
    radio = float(input("Introduce el radio del circulo: "))
    print(f"El area del circulo es: {radio ** 2 * math.pi }")
else:
    base = int(input("Introduce la base del triangulo: "))
    height = int(input("Introduce la altura del triangulo: "))
    print(f"El area del triangulo  es: { base * height / 2}")

space_separator()

# EJERCICIO 11 #
print("EJERCICIO 11: ")
# TODO 11:


space_separator()

# EJERCICIO 12 #
print("EJERCICIO 12: Dias Mes")
print("Escribe una fecha una Fecha:")
day = int(input("Dia: "))
month = int(input("Mes: "))
year = int(input("Año: "))

month_31d = [1, 3, 5, 7, 8, 10, 12]
month_30d = [4, 6, 9, 11]

if month in month_31d:
    if day <= 31:
        print("Fecha valida")
    else:
        print("Fecha no valida")


if month in month_30d:
    if day <= 30:
        print("Fecha valida")
    else:
        print("Fecha no valida")

if month == 2:
    if not year % 4 and (year % 100 or not year % 400):
        if day <= 29:
            print("Fecha valida")
        else:
            print("Fecha no valida")
    elif day <= 28:
        print("Fecha valida")
    else:
        print("Fecha no valida")

space_separator()

# EJERCICIO 13 #
print("EJERCICIO 13: Suma numeros en un rango")
num_one = int(input("Introduce el primer numero: "))
num_two = int(input("Introduce el segundo numero: "))
while num_one >= num_two:
    print("El primer numero debe ser menor que el segundo!!!!!!")
    num_one = int(input("Introduce otra vez el primer numero: "))
    num_two = int(input("Introduce otra vez el segundo numero: "))

total_sum = 0
aux = num_one
while aux <= num_two:
    total_sum += aux
    aux += 1

print(f"La suma entre los numeros que van desde {num_one} hasta {num_two} es {total_sum}")


space_separator()

# EJERCICIO 14 #
print("EJERCICIO 14: Factorial")
number = int(input("Introduce un numero mayor a 0: "))
factorial = 1
while number <= 0:
    number = int(input("Introduce un numero mayor a 0: "))

for i in range(1, number + 1):
    factorial *= i

print(f"El factorial de {number} es {factorial}")

space_separator()

# EJERCICIO 15 #
print("EJERCICIO 15: Numeros multiplo de 7 entre 0 y 100")
for x in range(101):
    if (x % 7) == 0:
        print(f"{x} ", end="")

space_separator()

# EJERCICIO 16 #
print("EJERCICIO 16: Lista de 'X' numero(especificado) de palabras ")
count_word = int(input("Introduce el numero de palabras que vas a introducir: "))

list_word = [count_word for x in range(count_word)]

for x in range(count_word):
    list_word[x] = input(f"Palabra número {x + 1}: ")

print(list_word)

space_separator()

# EJERCICIO 17 #
print("EJERCICIO 17: Eliminar de una lista las palabras que hay en otra lista")
count_word_list1 = int(input("Numero de palabras de la primera lista: "))

list_one = [count_word_list1 for x in range(count_word_list1)]

for x in range(count_word_list1):
    list_one[x] = input(f"Palabra de la posición {x}: ")

count_word_list2 = int(input("Numero de palabras de la segunda lista: : "))

list_two = [count_word_list2 for x in range(count_word_list2)]

for x in range(count_word_list2):
    list_two[x] = input(f"Palabra de la posición {x}: ")

for x in list_one:
    for y in list_two:
        if x == y:
            list_one.remove(x)

print(list_one)

space_separator()


# EJERCICIO 18 #
print("EJERCICIO 18: Numero Primo")
number = int(input("Introduce un numero: "))

for i in range(2, number):
    if number % i == 0:
        print(f"El numero {number} no es primo")
        break
else:
    print(f"El numero {number} es primo")

space_separator()


# EJERCICIO 19 #
print("EJERCICIO 19: Numero Narcisista ")
narcissistic = input("Introduce un numero: ")
list_number = [int(x) ** len(narcissistic) for x in narcissistic]


if int(narcissistic) == sum(list_number):
    print(f"El numero {narcissistic} es un numero narcisista")
else:
    print(f"El numero {narcissistic} no es un numero narcisista")


space_separator()


# EJERCICIO 20 #
print("EJERCICIO 20: Lista de números dentro de un rango")
num_one = int(input("Introduce el primer numero: "))
num_two = int(input("Introduce el segundo numero: "))
while num_one >= num_two:
    print("El primer numero debe ser menor que el segundo!!!!!!")
    num_one = int(input("Introduce otra vez el primer numero: "))
    num_two = int(input("Introduce otra vez el segundo numero: "))

for x in range(num_one, num_two + 1):
    print(f"{x} ", end="")


space_separator()


# EJERCICIO 21 #
print("EJERCICIO 21: Lista de números dentro de un rango(Decreciente)")
num_one = int(input("Introduce el primer numero: "))
num_two = int(input("Introduce el segundo numero: "))
while num_one >= num_two:
    print("El primer numero debe ser menor que el segundo!!!!!!")
    num_one = int(input("Introduce otra vez el primer numero: "))
    num_two = int(input("Introduce otra vez el segundo numero: "))

for x in reversed(range(num_one, num_two + 1)):
    print(f"{x} ", end="")

space_separator()


# EJERCICIO 22 #
print("EJERCICIO 22: Lista de numeros raiz cuadrada dentro de un rango")
num_one = int(input("Introduce el primer numero: "))
num_two = int(input("Introduce el segundo numero: "))
while num_one >= num_two:
    print("El primer numero debe ser menor que el segundo!!!!!!")
    num_one = int(input("Introduce otra vez el primer numero: "))
    num_two = int(input("Introduce otra vez el segundo numero: "))

for x in range(num_one, num_two + 1):
    print(f"{x ** 2} ", end="")


space_separator()


# EJERCICIO 23 #
print("EJERCICIO 23: Lista de numeros pares cuadrada dentro de un rango ")
num_one = int(input("Introduce el primer numero: "))
num_two = int(input("Introduce el segundo numero: "))
while num_one >= num_two:
    print("El primer numero debe ser menor que el segundo!!!!!!")
    num_one = int(input("Introduce otra vez el primer numero: "))
    num_two = int(input("Introduce otra vez el segundo numero: "))

for x in range(num_one, num_two + 1):
    if x % 2 == 0:
        print(f"{x} ", end="")


space_separator()


# EJERCICIO 24 #
print("EJERCICIO 24: Escribir los 10 primeros números multiplo de 2")
# TODO 24: falta completar el ejercicio
lista = [n+2 for n in range(0, 20, 2)]
print(lista)

space_separator()


# EJERCICIO 25 #
print("EJERCICIO 25: ")
# TODO 25


space_separator()


# EJERCICIO 26 #
# TODO 26: revisar, no funciona correctamente, ejemplo: compuesto
print("EJERCICIO 26: Palabras Alfabética ")
listAlphabet = list(string.ascii_lowercase)
word = input("Introduce una palabra: ")
position_list = [listAlphabet.index(letter) for letter in word]
min_index = position_list.index(min(position_list))


if min_index == 0:
    print(f"La palabra {word} es Alfabética")
else:
    print(f"La palabra {word}  no es Alfabética")


space_separator()


# EJERCICIO 27 #
print("EJERCICIO 27: Cadena Palíndroma")
word = input("Introduce una palabra: ")
if word.replace(" ", "") == word[::-1].replace(" ", ""):
    print(f"La cadena '{word}' es palíndroma")
else:
    print(f"La cadena '{word}' no es palíndroma")


space_separator()


# EJERCICIO 28 #
print("EJERCICIO 28: validador DNI ")
valid_dni_letter = "TRWAGMYFPDXBNJZSQVHLCKE"
valid_nie_letter = {"X": 0, "Y": 1, "Z": 2}
dni = input("Introduzca su DNI: ")

while len(dni) < 9:
    print("El numero de identificación fiscal debe tener 9 dígitos")
    dni = input("Introduzca su DNI: ")


if not dni[0].isnumeric():
    strDni = [num for num in dni]
    strDni.remove(dni[0])
    nie_letter = valid_nie_letter[dni[0].upper()]
    strDni.insert(0, str(nie_letter))
    dni = ''.join(strDni);

num_dni = int(dni[0:len(dni) - 1])
letter_dni = dni[-1].upper()
letter = num_dni % 23

if letter_dni == valid_dni_letter[letter]:
    print(f"El DNI {dni} es valido")
else:
    print(f"El DNI {dni} no es un DNI valido")

space_separator()


# EJERCICIO 29 #
print("EJERCICIO 29: Lectura de Fichero")
list_word = []
with open('exe29.txt') as file:
    for line in file:
        for word in line.split():
            if not list_word.__contains__(word.lower()):
                list_word.append(word.lower())

print(sorted(list_word))



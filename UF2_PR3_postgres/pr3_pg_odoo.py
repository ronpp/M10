import psycopg2


# BEGIN DB METHODS AND CONNECTION
def connectio_db():
    PSQL_HOST = 'localhost'
    PSQL_PORT = '5432'
    PSQL_USER = 'root'
    PSQL_PASS = 'root'
    PSQL_DB = 'Bddemo'
    connection_address = f"host={PSQL_HOST} port={PSQL_PORT} user={PSQL_USER} password={PSQL_PASS} dbname={PSQL_DB}"
    connection = psycopg2.connect(connection_address)
    return connection


def select_query(query):
    conn = connectio_db()
    cursor = conn.cursor()
    cursor.execute(query)
    data = cursor.fetchall()
    cursor.close()
    conn.close()
    return data


def sql_query(query):
    conn = None
    result = None
    try:
        conn = connectio_db()
        cursor = conn.cursor()
        cursor.execute(query)
        result = cursor.statusmessage
        cursor.close()
        conn.commit()
    except(Exception, psycopg2.DatabaseError) as error:
        conn.rollback()
        result = error
    finally:
        if conn is not None:
            conn.close()
    return result

#   END  DB METHODS#


# EXERCISE 1
def show_user_res_users():
    sql = 'SELECT id, active, login, create_date FROM res_users'
    users = select_query(sql)
    for user in users:
        print(f"ID: {user[0]}, isActive: {user[1]}, login: {user[2]}, create date: {user[3]}")


# EXERCISE 2
def create_db_prueba():
    sql = """
    CREATE TABLE IF NOT EXISTS prueba(
        id SERIAL PRIMARY KEY,
        text_demo varchar(10) 
    )
    """
    print("Table Creation: ")
    print(sql_query(sql))

    print("Table Insertion:")
    for i in range(1, 6):
        data = f"data num{i}"
        sql = f"INSERT INTO prueba(text_demo) VALUES ('{data}')"
        print(sql_query(sql))


# EXERCISE 3
def show_data_db_prueba():
    sql_ids = "SELECT id FROM prueba"
    print("IDs: ")
    for row_id in select_query(sql_ids):
        print(row_id[0])

    user_opt = input("Enter num id: ")
    sql_selected_id = f"SELECT * FROM prueba WHERE id = {int(user_opt)}"
    print("Data from selected Id: ")
    print(select_query(sql_selected_id))


# EXERCISE 4
def update_data_db_prueba():
    sql_ids = "SELECT id FROM prueba"
    print("IDs: ")
    for row_id in select_query(sql_ids):
        print(row_id[0])

    user_opt = input("Enter num id: ")
    sql_selected_id = f"SELECT * FROM prueba WHERE id = {int(user_opt)}"
    print("Data from Selected Id: ")
    print(select_query(sql_selected_id))
    new_value = input("Enter the new value:")
    sql_update_query = f"UPDATE prueba SET text_demo = '{new_value}' WHERE id = {int(user_opt)}"
    print(sql_query(sql_update_query))
    print("The new value is:")
    print(select_query(sql_selected_id))


# Utils method
def space_separator(nun_spaces: int):
    for s in range(nun_spaces):
        print()


# MAIN Menu
def main_menu():
    title_options = "Select an option and press Enter"
    delimiter = len(title_options) * "="
    print(f"{delimiter}\n{title_options}\n{delimiter}")
    menu_options = ["Show users in res_users", "Create DB prueba", "Show data db prueba",
                    "Modify data DB prueba", "Quit"]
    for option in menu_options:
        index = menu_options.index(option) + 1
        print(f"\t{index}. {option}")
    return int(input("Enter an option: "))


if __name__ == "__main__":
    opt = main_menu()
    while opt != 5:
        print()
        if opt == 1:
            space_separator(1)
            show_user_res_users()
        elif opt == 2:
            space_separator(1)
            create_db_prueba()
        elif opt == 3:
            space_separator(1)
            show_data_db_prueba()
        elif opt == 4:
            space_separator(1)
            update_data_db_prueba()

        opt = main_menu()
    space_separator(1)
    print("Bye Bye ")

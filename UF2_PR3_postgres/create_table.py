from db import sql_query


SQL = """
CREATE TABLE IF NOT EXISTS prueba(
    id SERIAL PRIMARY KEY,
    text_demo varchar(10) 
)
"""
print("Table Creation: ")
print(sql_query(SQL))

print("Table Insertion:")
for i in range(1, 6):
    data = f"data num{i}"
    sql = f"INSERT INTO prueba(text_demo) VALUES ('{data}')"
    print(sql_query(sql))

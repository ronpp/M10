import psycopg2


def connectio_db():
    PSQL_HOST = 'localhost'
    PSQL_PORT = '5432'
    PSQL_USER = 'root'
    PSQL_PASS = 'root'
    PSQL_DB = 'Bddemo'
    connection_address = f"host={PSQL_HOST} port={PSQL_PORT} user={PSQL_USER} password={PSQL_PASS} dbname={PSQL_DB}"
    connection = psycopg2.connect(connection_address)
    return connection


def select_query(query):
    conn = connectio_db()
    cursor = conn.cursor()
    cursor.execute(query)
    data = cursor.fetchall()
    cursor.close()
    conn.close()
    return data


def sql_query(query):
    conn = None
    result = None
    try:
        conn = connectio_db()
        cursor = conn.cursor()
        cursor.execute(query)
        result = cursor.statusmessage
        cursor.close()
        conn.commit()
    except(Exception, psycopg2.DatabaseError) as error:
        conn.rollback()
        result = error
    finally:
        if conn is not None:
            conn.close()
    return result







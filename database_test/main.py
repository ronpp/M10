import psycopg2

PSQL_HOST = 'localhost'
PSQL_PORT = '5432'
PSQL_USER = 'root'
PSQL_PASS = 'root'
PSQL_DB = 'examenPPRA'

connection_address = f"host={PSQL_HOST} port={PSQL_PORT} user={PSQL_USER} password={PSQL_PASS} dbname={PSQL_DB}"
connection = psycopg2.connect(connection_address)
cursor = connection.cursor()
SQL = 'SELECT * FROM res_users'
cursor.execute(SQL)

all_values = cursor.fetchall()

cursor.close()
connection.close()
print('Get Values: ', all_values)

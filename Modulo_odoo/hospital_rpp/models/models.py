# -*- coding: utf-8 -*-

# from odoo import models, fields, api


# class hospital_rpp(models.Model):
#     _name = 'hospital_rpp.hospital_rpp'
#     _description = 'hospital_rpp.hospital_rpp'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100

from odoo import models, fields


class Patient(models.Model):
    _name = 'hospital.hospital_rpp_patient'
    _description = 'Patient Record'
    _rec_name = 'nom'
    _order = "nom"

    nom = fields.Char(required=True, string='Nom')
    cognom = fields.Char(required=True, string='Cognom')
    dni = fields.Char(required=True, string='DNI')
    genere = fields.Selection([('male', 'Masculí'), ('female', 'Femení')], 'Gènere')
    adreca = fields.Char(required=True, string='Adreça')
    data_neixement = fields.Date(required=True, string='Data de neixement', default=fields.datetime.now())
    nacionalitat = fields.Many2one('res.country', string='Nacionalitat', required=True,
                                   default=lambda self: self.env['res.country'].search([]))
    ingres = fields.Boolean(required=True, string='Ingrès ?', default=False)
    data_ingres = fields.Date(string='Data d\'ingrès', default=fields.datetime.now())
    informacio = fields.Text(string='Informació')
    asseguranca = fields.Char(string='Assegurança')

    prova_ids = fields.One2many('hospital.hospital_rpp_test', 'pacient_id', string='prova_ids')
    doctor_rel = fields.Many2one('hospital.hospital_rpp_doctor', string='doctor_rel')


class Doctor(models.Model):
    _name = 'hospital.hospital_rpp_doctor'
    _description = 'Doctor Record'
    _rec_name = 'nom'
    _order = "nom"

    nom = fields.Char(required=True, string='Nom')
    cognom = fields.Char(required=True, string='Cognom')
    dni = fields.Char(required=True, string='DNI')
    genere = fields.Selection([('male', 'Masculí'), ('female', 'Femení')], 'Gènere')
    adreca = fields.Char(required=True, string='Adreça')
    anys_experiencia = fields.Integer(string="Anys d'experiècia")

    especialitat_id = fields.Many2one('hospital.hospital_rpp_speciality', ondelete='Set null',string='Especialitat')
    edifici = fields.Char(string='Edifici', related='especialitat_id.edifici')


class Speciality(models.Model):
    _name = 'hospital.hospital_rpp_speciality'
    _description = 'Speciality Record'
    _rec_name = 'nom'
    _order = "nom"

    nom = fields.Char(required=True, string='Nom')
    edifici = fields.Char(required=True, string='Edifici')
    metge_ids = fields.One2many('hospital.hospital_rpp_doctor', 'edifici', string='metge_ids')


class Test(models.Model):
    _name = 'hospital.hospital_rpp_test'
    _description = 'Medical Tests Record'
    _rec_name = 'nom'
    _order = "nom"

    nom = fields.Char(required=True, string='Nom')
    codi = fields.Selection([('Radio', 'Radiologia'), ('Extr', 'Extraccions'),('Electro', 'Electro')], 'Tipus')
    data_prova = fields.Date(string='Data prova', default=fields.datetime.now())
    pacient_id = fields.Many2one('hospital.hospital_rpp_patient',string='pacient_id')
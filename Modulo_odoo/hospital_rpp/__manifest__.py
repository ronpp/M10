# -*- coding: utf-8 -*-
{
    'name': "hospitalRPP",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Ronny",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Extra Tools',
    'version': '0.1',
    'summary': 'module for managing the Hospital',
    'sequence': '10',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'data/patient.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo_patient.xml',
        'demo/demo_test.xml',
        'demo/demo_speciality.xml',
        'demo/demo_doctor.xml',

    ],

    'installable': True,
    'application': True,
    'auto_install': False
}

class Client:
    def __init__(self, id_client: int, name: str, lastname: str, phone: str, email: str, address: str, city: str):
        self.id = id_client
        self.name = name
        self.lastname = lastname
        self.phone = phone
        self.email = email
        self.address = address
        self.city = city

    def __repr__(self):
        return repr(
            f"Id: {self.id}, name: {self.name}, lastname: {self.lastname}, phone: {self.phone}, email: {self.email}, "
            f"Address: {self.address}, city: {self.city}")

def show_main_menu():
    title_options = "Select an option and press Enter"
    delimiter = len(title_options) * "="
    print("MAIN MENU")
    print(f"{delimiter}\n{title_options}\n{delimiter}")
    menu_options = ["Add Cliet", "Delete Client", "Consult Client", "Modify field of Client(*)", "Quit"]
    for option in menu_options:
        index = menu_options.index(option) + 1
        print(f"\t{index}. {option}")
    return int(input("Enter an option: "))


def show_consult_menu():
    title_options = "Select an option and press Enter"
    delimiter = len(title_options) * "="
    print("QUERY MENU")
    print(f"{delimiter}\n{title_options}\n{delimiter}")
    menu_options = ["Find Client by ID",
                    "Find Client by Name",
                    "Find Client by Lastname",
                    "List All Clients",
                    "List all Clients by name(*)",
                    "Back"]
    for option in menu_options:
        index = menu_options.index(option) + 1
        print(f"\t{index}. {option}")
    return int(input("Enter an option: "))

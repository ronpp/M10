from client import Client


class NoteBook:
    def __init__(self):
        self.client_list = []
        self.id_client = 0

    def get_client_list(self):
        return self.client_list

    def add_client(self, name, lastname, phone, email, address, city):
        self.id_client += 1
        self.client_list.append(Client(self.id_client, name, lastname, phone, email, address, city))

    def delete_client(self, client_id):
        for client in self.client_list:
            if client.id == int(client_id):
                self.client_list.remove(client)

    def find_client_by_id(self, client_id):
        for client in self.client_list:
            if client.id == int(client_id):
                return client
        return f"the client with id '{client_id}' don't exist"

    def find_client_by_name(self, name):
        for client in self.client_list:
            if client.name.lower() == name.lower():
                return client
        return f"the client with name '{name}' don't exist"

    def find_client_by_lastname(self, lastname):
        for client in self.client_list:
            if client.lastname.lower() == lastname.lower():
                return client
        return f"the client with last '{lastname}' don't exist"

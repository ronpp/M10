import menu
from notebook import NoteBook

QUIT_MAIN_MENU = 5
QUIT_CONSULT_MENU = 6


def space_separator(nun_spaces: int):
    for s in range(nun_spaces):
        print()


space_separator(1)
notebook = NoteBook()
option_main_menu = menu.show_main_menu()
while option_main_menu != QUIT_MAIN_MENU:

    if option_main_menu == 1:
        space_separator(1)
        print("New Client")
        name = input("client name: ")
        lastname = input("client lastname: ")
        phone = input("client phone: ")
        email = input("client email: ")
        address = input("client address: ")
        city = input("client city: ")
        notebook.add_client(name, lastname, phone, email, address, city)
        space_separator(1)
        print("Client added!!")
        print(f"\t{notebook.find_client_by_name(name)}")

    elif option_main_menu == 2:
        space_separator(1)
        print("ELIMINATION OF CLIENT!! << WATCH OUT >>")
        print("Client List:")
        for client in notebook.get_client_list():
            print(client)
        space_separator(1)
        client_id = input("Enter the client ID: ")
        notebook.delete_client(client_id)
        print(f"The client with id: {client_id} has been deleted")

    elif option_main_menu == 3:
        space_separator(1)
        option_consult_menu = menu.show_consult_menu()

        while option_consult_menu != QUIT_CONSULT_MENU:

            if option_consult_menu == 1:
                space_separator(1)
                client_id = input("Enter the client ID: ")
                print(notebook.find_client_by_id(client_id))

            elif option_consult_menu == 2:
                space_separator(1)
                client_name = input("Enter the client name: ")
                print(notebook.find_client_by_name(client_name))

            elif option_consult_menu == 3:
                space_separator(1)
                client_lastname = input("Enter the client lastname: ")
                print(notebook.find_client_by_lastname(client_lastname))

            elif option_consult_menu == 4:
                space_separator(1)
                print("Client List:")
                for client in notebook.get_client_list():
                    print(client)

            elif option_consult_menu == 5:
                space_separator(1)
                print("Client List(Sorted by name):")
                for client in sorted(notebook.get_client_list(), key=lambda client_item: client.name):
                    print(client)

            else:
                space_separator(1)
                print("Invalid option!!!")

            space_separator(2)
            option_consult_menu = menu.show_consult_menu()

    elif option_main_menu == 4:
        space_separator(1)
        client_id = input("Enter the client ID: ")
        client_to_mod = notebook.find_client_by_id(client_id)
        client_pos = notebook.get_client_list().index(client_to_mod)
        print(f"\tClient: {client_to_mod}")
        space_separator(1)
        option_field = input("What field do you want to modify { name, lastname, phone, email ,address , city }: ")
        field = input("Enter the new value: ")

        if option_field == "name":
            client_to_mod.name = field
        elif option_field == "lastname":
            client_to_mod.lastname = field
        elif option_field == "phone":
            client_to_mod.phone = field
        elif option_field == "email":
            client_to_mod.email = field
        elif option_field == "address":
            client_to_mod.address = field
        elif option_field == "city":
            client_to_mod.city = field
        notebook.get_client_list()[client_pos] = client_to_mod

    else:
        space_separator(1)
        print("Invalid option!!!")
    space_separator(2)
    option_main_menu = menu.show_main_menu()


space_separator(1)
print("Goodbye, Thanks for visiting us!!")
